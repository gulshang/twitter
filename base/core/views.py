# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_GET, require_POST
from django.db.models import Q

from .models import User, Tweet, UserFollowMap
from .forms import SignUpForm, TweetForm


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


@login_required
@require_GET
def home(request):
    query = request.GET.get('q')
    tweet_form = TweetForm()
    return render(request, template_name='core/home.html', context={
        'tweets': get_followed_tweets(request.user, query=query),
        'tweet_form': tweet_form,
    })


@login_required
@require_GET
def followed_tweets(request):
    return JsonResponse({
        'success': True, 'tweets': get_followed_tweets(request.user)
    })


@login_required
def tweet_api(request, tweet_id=None):
    if request.GET:
        tweet = get_object_or_404(Tweet, pk=tweet_id)
        return JsonResponse({'success': True, 'tweet': tweet.mini_json()})
    data = {'text': request.POST['text'], 'user': request.user.id}
    form = TweetForm(data=data)
    assert form.is_valid()
    form.save()
    return HttpResponseRedirect('/')


@login_required
@require_POST
def follow(request, to_follow_id):
    to_follow = get_object_or_404(User, pk=to_follow_id)
    UserFollowMap.objects.get_or_create(user=request.user, following=to_follow)
    return JsonResponse({'success': True})


def get_followed_tweets(user, query=None):
    if query:
        users_ids = User.objects.filter(
            Q(first_name__icontains=query) | Q(last_name__icontains=query) | Q(username__icontains=query)
        ).values_list('id', flat=True)
    else:
        users_ids = UserFollowMap.objects.filter(user_id=user.id).values_list('following_id', flat=True)
    tweets = Tweet.objects.filter(user_id__in=users_ids)
    rdata = []
    for t in tweets:
        mj = t.mini_json()
        mj.update({'is_follower': user.is_follower(tweet_user_id=mj['user']['pk'])})
        rdata.append(mj)
    return rdata
