from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^tweet/$', views.tweet_api, name='tweet_api'),
    url(r'^follow/(?P<to_follow_id>[\d]+)/$', views.follow, name='tweet_api'),
]