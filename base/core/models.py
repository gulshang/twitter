# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.lru_cache import lru_cache


class UserFollowMap(models.Model):
    user = models.ForeignKey('User', related_name='object_for_user')
    following = models.ForeignKey('User', related_name='object_for_following')

    class Meta:
        unique_together = ('user', 'following')


class User(AbstractUser):
    pass

    @property
    def screen_name(self):
        if self.first_name:
            return (self.first_name + ' ' + self.last_name).strip()
        return self.username

    def mini_json(self):
        return {
            'pk': self.pk,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'username': self.username,
            'screen_name': self.screen_name,
        }

    def is_follower(self, tweet_user_id):
        return UserFollowMap.objects.filter(user=self, following_id=tweet_user_id).exists()


class Tweet(models.Model):
    text = models.CharField(max_length=140)
    user = models.ForeignKey('User', related_name='tweets', db_index=True)
    created_on = models.DateTimeField(auto_now_add=True, db_index=True)
    modified_on = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_on']

    def mini_json(self):
        return {
            'pk': self.pk,
            'user': self.user.mini_json(),
            'text': self.text,
            'created_on': self.created_on,
        }
