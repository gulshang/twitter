1. What technologies would you use to build out this platform? Please tell us the
languages, databases, tools / servers you would use to build out the above platform.

Ans.
Language - Python
Framework - Django
Database - MySQL


2. Write the schema of your database that is going to store the data. We want to see this in
detail to see where the all the different information will be stored
--


3. Write a function/API that will return all the tweets to show on the dashboard of a
particular user. Please make sure to return the data in json.
--


4. How much can the system you have built scale up to? What are the limiting factors of
your system and when will it start failing?



5. BONUS: write the frontend code for the textbox where you can type your tweet and the
submit button that will send the request to publish this tweet.