import operator
import re
from collections import defaultdict
from urllib2 import urlopen

count_map = defaultdict(int)
line_iterator = urlopen('https://s3.ap-south-1.amazonaws.com/haptikinterview/chats.txt')

for line in line_iterator:
    if not (line and line.strip()):
        continue
    count_map[re.findall("<(.*?)>", line)[0]] += 1

sorted_list = sorted(count_map.items(), key=operator.itemgetter(1), reverse=True)

for item in sorted_list[:3]:
    print item[0]