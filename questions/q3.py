def can_create(list_of_strings, input_string):
    index = 0
    length = len(list_of_strings)
    while input_string and index < length:
        input_string = input_string.replace(list_of_strings[index], '')
        index += 1

    return not bool(input_string)